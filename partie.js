// Déclaration des variables utiles au jeu.
var carte1Joueur = 0;
var carte2Joueur = 0;
var carte3Joueur = 0;
var carte4Joueur = 0;
var carte5Joueur = 0;
var carte6Joueur = 0;
var carte7Joueur = 0;
var carte8Joueur = 0;
var carte9Joueur = 0;
var carte10Joueur = 0;
var valeurTotCarteJoueur = 0;
var carte1Croupier = 0;
var carte2Croupier = 0; // valeur caché au début.
var carte3Croupier = 0;
var carte4Croupier = 0;
var carte5Croupier = 0;
var carte6Croupier = 0;
var carte7Croupier = 0;
var carte8Croupier = 0;
var carte9Croupier = 0;
var carte10Croupier = 0;
var valeurTotCarteCroupier = 0;
var carte = 0;
var valTotCrouHypo = 0;
function tirerUneCarte() {
    carte = Math.floor(Math.random() * (10 - 1 + 1)) + 1;
}
function tirageDeCartes() {
    tirerUneCarte();
    carte1Joueur = carte;
    tirerUneCarte();
    carte2Joueur = carte;
    valeurTotCarteJoueur += carte1Joueur;
    valeurTotCarteJoueur += carte2Joueur;
    tirerUneCarte();
    carte1Croupier = carte;
    tirerUneCarte();
    carte2Croupier = carte;
    valeurTotCarteCroupier += carte1Croupier;
    valeurTotCarteCroupier += carte2Croupier;
}
function joueurAutreCarte() {
    switch (0) {
        case carte3Joueur:
            tirerUneCarte();
            carte3Joueur = carte;
            valeurTotCarteJoueur += carte3Joueur;
            verifValeurCarteJoueur(); // Appel de la fonction permettant de vérifier les valeurs des cartes du joueur.
            break;
        case carte4Joueur:
            tirerUneCarte();
            carte4Joueur = carte;
            valeurTotCarteJoueur += carte4Joueur;
            verifValeurCarteJoueur(); // Appel de la fonction permettant de vérifier les valeurs des cartes du joueur.
            break;
        case carte5Joueur:
            tirerUneCarte();
            carte5Joueur = carte;
            valeurTotCarteJoueur += carte5Joueur;
            verifValeurCarteJoueur(); // Appel de la fonction permettant de vérifier les valeurs des cartes du joueur.
            break;
        case carte6Joueur:
            tirerUneCarte();
            carte6Joueur = carte;
            valeurTotCarteJoueur += carte6Joueur;
            verifValeurCarteJoueur(); // Appel de la fonction permettant de vérifier les valeurs des cartes du joueur.
            break;
        case carte7Joueur:
            tirerUneCarte();
            carte7Joueur = carte;
            valeurTotCarteJoueur += carte7Joueur;
            verifValeurCarteJoueur(); // Appel de la fonction permettant de vérifier les valeurs des cartes du joueur.
            break;
        case carte8Joueur:
            tirerUneCarte();
            carte8Joueur = carte;
            valeurTotCarteJoueur += carte8Joueur;
            verifValeurCarteJoueur(); // Appel de la fonction permettant de vérifier les valeurs des cartes du joueur.
            break;
        case carte9Joueur:
            tirerUneCarte();
            carte9Joueur = carte;
            valeurTotCarteJoueur += carte9Joueur;
            verifValeurCarteJoueur(); // Appel de la fonction permettant de vérifier les valeurs des cartes du joueur.
            break;
        case carte10Joueur:
            tirerUneCarte();
            carte10Joueur = carte;
            valeurTotCarteJoueur += carte10Joueur;
            verifValeurCarteJoueur(); // Appel de la fonction permettant de vérifier les valeurs des cartes du joueur.
            break;
    }
}
function croupierAutreCarte() {
    tirerUneCarte();
    valTotCrouHypo = 0;
    valTotCrouHypo += valeurTotCarteCroupier;
    valTotCrouHypo += carte;
    if (valTotCrouHypo < 17) {
        if (carte < 2) {
            if ((16 < valTotCrouHypo) && (valTotCrouHypo < 22)) {
                valeurTotCarteCroupier += 11;
            }
            if (valTotCrouHypo < 17) {
                valeurTotCarteCroupier += carte;
            }
        }
        if (carte > 1) {
            valeurTotCarteCroupier += carte;
        }
    }
    if ((valTotCrouHypo > 16) && (carte > 1)) {
        valeurTotCarteCroupier += carte;
        if (valeurTotCarteCroupier > 21) {
            console.log("Partie finie :");
            console.log("La valeur du total des cartes du croupier est : ".concat(valeurTotCarteCroupier));
            console.log("le croupier a perdu");
            console.log("La valeur du total des cartes du joueur est : ".concat(valeurTotCarteJoueur));
            console.log("Le joueur a gagné");
            console.log("Pour d\u00E9marrer une nouvelle partie, taper : nouvellePartie();"); // Appel de la fonction permettant de démarrer une partie.
        }
        if (valeurTotCarteCroupier < 22) {
            if (valeurTotCarteCroupier > valeurTotCarteJoueur) {
                console.log("Partie finie :");
                console.log("La valeur du total des cartes du croupier est : ".concat(valeurTotCarteCroupier));
                console.log("le croupier a gagné");
                console.log("La valeur du total des cartes du joueur est : ".concat(valeurTotCarteJoueur));
                console.log("Le joueur a perdu");
                console.log("Pour d\u00E9marrer une nouvelle partie, taper : nouvellePartie();"); // Appel de la fonction permettant de démarrer une partie.
            }
            if (valeurTotCarteCroupier < valeurTotCarteJoueur) {
                console.log("Partie finie :");
                console.log("La valeur du total des cartes du croupier est : ".concat(valeurTotCarteCroupier));
                console.log("le croupier a perdu");
                console.log("La valeur du total des cartes du joueur est : ".concat(valeurTotCarteJoueur));
                console.log("Le joueur a gagné");
                console.log("Pour d\u00E9marrer une nouvelle partie, taper : nouvellePartie();"); // Appel de la fonction permettant de démarrer une partie.
            }
        }
    }
    if (valeurTotCarteCroupier < 17) {
        console.log("La valeur du total des cartes du croupier est : ".concat(valeurTotCarteCroupier));
        console.log("Taper : croupierAutreCarte();"); // Appel de la fonction permettant faire tirer une autre carte au croupier.
    }
    if (valeurTotCarteCroupier > 21) {
        console.log("Partie finie :");
        console.log("La valeur du total des cartes du croupier est : ".concat(valeurTotCarteCroupier));
        console.log("le croupier a perdu");
        console.log("La valeur du total des cartes du joueur est : ".concat(valeurTotCarteJoueur));
        console.log("Le joueur a gagné");
        console.log("Pour d\u00E9marrer une nouvelle partie, taper : nouvellePartie();"); // Appel de la fonction permettant de démarrer une partie.
    }
}
function revelerCarteCroupier() {
    console.log("Joueur a les valeurs de cartes : ".concat(carte1Joueur, " et ").concat(carte2Joueur));
    // If permettant de révéler les cartes tirées du joueur.
    if (carte3Joueur != 0) {
        console.log("Joueur a la valeur de sa troisi\u00E8me carte \u00E0 : ".concat(carte3Joueur));
    }
    if (carte4Joueur != 0) {
        console.log("Joueur a la valeur de sa quatri\u00E8me carte \u00E0 : ".concat(carte4Joueur));
    }
    if (carte5Joueur != 0) {
        console.log("Joueur a la valeur de sa cinqui\u00E8me carte \u00E0 : ".concat(carte5Joueur));
    }
    if (carte6Joueur != 0) {
        console.log("Joueur a la valeur de sa sixi\u00E8me carte \u00E0 : ".concat(carte6Joueur));
    }
    if (carte7Joueur != 0) {
        console.log("Joueur a la valeur de sa septi\u00E8me carte \u00E0 : ".concat(carte7Joueur));
    }
    if (carte8Joueur != 0) {
        console.log("Joueur a la valeur de sa huiti\u00E8me carte \u00E0 : ".concat(carte8Joueur));
    }
    if (carte9Joueur != 0) {
        console.log("Joueur a la valeur de sa neuvi\u00E8me carte \u00E0 : ".concat(carte9Joueur));
    }
    if (carte10Joueur != 0) {
        console.log("Joueur a la valeur de sa dixi\u00E8me carte \u00E0 : ".concat(carte10Joueur));
    }
    console.log("La valeur du total des cartes du joueur est : ".concat(valeurTotCarteJoueur));
    console.log("Croupier a les cartes : ".concat(carte1Croupier, " et ").concat(carte2Croupier));
    console.log("La valeur du total des cartes du croupier est : ".concat(valeurTotCarteCroupier));
    if (valeurTotCarteCroupier >= 17) {
        if (valeurTotCarteCroupier > valeurTotCarteJoueur) {
            console.log("Partie finie :");
            console.log("La valeur du total des cartes du croupier est : ".concat(valeurTotCarteCroupier));
            console.log("le croupier a gagné");
            console.log("La valeur du total des cartes du joueur est : ".concat(valeurTotCarteJoueur));
            console.log("Le joueur a perdu");
            console.log("Pour d\u00E9marrer une nouvelle partie, taper : nouvellePartie();"); // Appel de la fonction permettant de démarrer une partie.
        }
        if (valeurTotCarteCroupier < valeurTotCarteJoueur) {
            console.log("Partie finie :");
            console.log("La valeur du total des cartes du croupier est : ".concat(valeurTotCarteCroupier));
            console.log("le croupier a perdu");
            console.log("La valeur du total des cartes du joueur est : ".concat(valeurTotCarteJoueur));
            console.log("Le joueur a gagné");
            console.log("Pour d\u00E9marrer une nouvelle partie, taper : nouvellePartie();"); // Appel de la fonction permettant de démarrer une partie.
        }
    }
    if (valeurTotCarteCroupier <= 16) {
        console.log("Taper : croupierAutreCarte();"); // Appel de la fonction permettant de faire tirer une autre carte au croupier suivant les conditions.
    }
}
function quellesCartes() {
    console.log("Joueur a les valeurs de cartes : ".concat(carte1Joueur, " et ").concat(carte2Joueur));
    console.log("La valeur du total des cartes du joueur est : ".concat(valeurTotCarteJoueur));
    console.log("Croupier a les cartes : ".concat(carte1Croupier, " et carte \u00E0 valeur cach\u00E9e"));
    console.log("Si le joueur veut une autre carte, \u00E9crivez : \"joueurAutreCarte();\""); // Appel de la fonction par le joueur lui permettant de tirer une autre carte.
    console.log("Si le joueur ne veut pas une autre carte, \u00E9crivez : \"revelerCarteCroupier();\""); // Appel de ma fonction par le joueur lui permettant de révéler la deuxième carte du croupier.
}
function deroulerPartiePremierRound() {
    tirageDeCartes(); // Appel de la fonction permettant de tirer des cartes de valeurs aléatoires.
    quellesCartes(); // Appel de la fonction permettant de savoir quelles sont les valeurs des cartes tirées, suivant les modalités du jeu.
}
function compterAsJoueur() {
    valeurTotCarteJoueur += 10;
    carte += 100;
    verifValeurCarteJoueur();
}
function verifValeurCarteJoueur() {
    console.log("Joueur a les valeurs de cartes : ".concat(carte1Joueur, " et ").concat(carte2Joueur));
    // If permettant de révéler les cartes tirées du joueur.
    if (carte3Joueur != 0) {
        console.log("Joueur a la valeur de sa troisi\u00E8me carte \u00E0 : ".concat(carte3Joueur));
    }
    if (carte4Joueur != 0) {
        console.log("Joueur a la valeur de sa quatri\u00E8me carte \u00E0 : ".concat(carte4Joueur));
    }
    if (carte5Joueur != 0) {
        console.log("Joueur a la valeur de sa cinqui\u00E8me carte \u00E0 : ".concat(carte5Joueur));
    }
    if (carte6Joueur != 0) {
        console.log("Joueur a la valeur de sa sixi\u00E8me carte \u00E0 : ".concat(carte6Joueur));
    }
    if (carte7Joueur != 0) {
        console.log("Joueur a la valeur de sa septi\u00E8me carte \u00E0 : ".concat(carte7Joueur));
    }
    if (carte8Joueur != 0) {
        console.log("Joueur a la valeur de sa huiti\u00E8me carte \u00E0 : ".concat(carte8Joueur));
    }
    if (carte9Joueur != 0) {
        console.log("Joueur a la valeur de sa neuvi\u00E8me carte \u00E0 : ".concat(carte9Joueur));
    }
    if (carte10Joueur != 0) {
        console.log("Joueur a la valeur de sa dixi\u00E8me carte \u00E0 : ".concat(carte10Joueur));
    }
    if (valeurTotCarteJoueur > 21) {
        console.log("Partie finie :");
        console.log("le croupier a gagné");
        console.log("La valeur du total des cartes du joueur est : ".concat(valeurTotCarteJoueur));
        console.log("Le joueur a perdu");
        console.log("Pour d\u00E9marrer une nouvelle partie, taper : nouvellePartie();"); // Appel de la fonction permettant de démarrer une partie
    }
    if (valeurTotCarteJoueur === 21) {
        console.log("Partie finie :");
        console.log("le croupier a perdu");
        console.log("La valeur du total des cartes du joueur est : ".concat(valeurTotCarteJoueur));
        console.log("Le joueur a gagné");
        console.log("Pour d\u00E9marrer une nouvelle partie, taper : nouvellePartie();"); // Appel de la fonction permettant de démarrer une partie
    }
    if (valeurTotCarteJoueur < 21) {
        console.log("La valeur du total des cartes du joueur est : ".concat(valeurTotCarteJoueur));
        console.log("Croupier a les cartes : ".concat(carte1Croupier, " et carte \u00E0 valeur cach\u00E9e"));
        console.log("Si le joueur veut une autre carte, \u00E9crivez : \"joueurAutreCarte();\""); // Appel de la fonction par le joueur lui permettant de tirer une autre carte.
        if (carte1Joueur === 1) {
            console.log("Le joueur a tir\u00E9e un as, la valeur total des cartes du joueur est compt\u00E9e comme si la carte valait 1");
            console.log("Si le joueur veut compter l'as comme un 11 (donc rajout\u00E9 : 10), taper : compterAsJoueur();"); // Appel de la fonction permettant au joueur de choisir la valeur de l'as.
            console.log("Si non, \u00E9crivez : \"joueurAutreCarte();\""); // Appel de la fonction par le joueur lui permettant de tirer une autre carte.
        }
        if (carte === 1) {
            console.log("Le joueur a tir\u00E9e un as, la valeur total des cartes du joueur est compt\u00E9e comme si la carte valait 1");
            console.log("Si le joueur veut compter l'as comme un 11 (donc rajout\u00E9 : 10), taper : compterAsJoueur();"); // Appel de la fonction permettant au joueur de choisir la valeur de l'as.
            console.log("Si non, \u00E9crivez : \"joueurAutreCarte();\""); // Appel de la fonction par le joueur lui permettant de tirer une autre carte.
        }
        console.log("Si le joueur ne veut pas une autre carte, \u00E9crivez : \"revelerCarteCroupier();\""); // Appel de ma fonction par le joueur lui permettant de révéler la deuxième carte du croupier.
    }
}
function nouvellePartie() {
    // Les variables qui aurraient été eu une affectation de valeurs sont remises à zéro.
    carte1Joueur = 0;
    carte2Joueur = 0;
    carte3Joueur = 0;
    carte4Joueur = 0;
    carte5Joueur = 0;
    carte6Joueur = 0;
    carte7Joueur = 0;
    carte8Joueur = 0;
    carte9Joueur = 0;
    carte10Joueur = 0;
    valeurTotCarteJoueur = 0;
    carte1Croupier = 0;
    carte2Croupier = 0;
    carte4Croupier = 0;
    carte5Croupier = 0;
    carte6Croupier = 0;
    carte7Croupier = 0;
    carte8Croupier = 0;
    carte9Croupier = 0;
    carte10Croupier = 0;
    valeurTotCarteCroupier = 0;
    carte = 0;
    deroulerPartiePremierRound(); // les premières cartes sont tirées.
}
function ouvertureDepage() {
    console.log("Bienvenu sur le jeu du BlackJack");
    console.log("Pour d\u00E9marrer une nouvelle partie, taper : nouvellePartie();"); // Appel de la fonction permettant de démarrer une partie.
}
ouvertureDepage(); // Applet de la fonction en ouvrant la page ou en la rafraichissant.
