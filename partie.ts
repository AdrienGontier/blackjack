
// Déclaration des variables utiles au jeu.

let carte1Joueur = 0;
let carte2Joueur = 0;
let carte3Joueur = 0;
let carte4Joueur = 0;
let carte5Joueur = 0;
let carte6Joueur = 0;
let carte7Joueur = 0;
let carte8Joueur = 0;
let carte9Joueur = 0;
let carte10Joueur = 0;

let valeurTotCarteJoueur = 0

let carte1Croupier = 0;
let carte2Croupier = 0; // valeur caché au début.
let carte3Croupier = 0;
let carte4Croupier = 0;
let carte5Croupier = 0;
let carte6Croupier = 0;
let carte7Croupier = 0;
let carte8Croupier = 0;
let carte9Croupier = 0;
let carte10Croupier = 0;

let valeurTotCarteCroupier = 0;

let carte = 0;


let valTotCrouHypo = 0;



function tirerUneCarte() { // Fonction permettant de tirer une carte avec une valeur aléatoire.
    carte = Math.floor(Math.random() * (10 - 1 +1)) + 1;
}


function tirageDeCartes() { // Fonction qui permet de tirer les 2 cartes du joueurs et les 2 cartes du croupier.
    
    tirerUneCarte();
    carte1Joueur = carte;
    
    tirerUneCarte();
    carte2Joueur = carte;
    
    valeurTotCarteJoueur += carte1Joueur;
    valeurTotCarteJoueur += carte2Joueur;
    
    tirerUneCarte();
    carte1Croupier = carte;
    
    tirerUneCarte();
    carte2Croupier = carte;
    
    valeurTotCarteCroupier += carte1Croupier;
    valeurTotCarteCroupier += carte2Croupier;
    
}

function joueurAutreCarte() { // Fonction permettant de tirer une autre carte.
    
    switch (0) {
        case carte3Joueur:
        tirerUneCarte();
        carte3Joueur = carte;
        valeurTotCarteJoueur += carte3Joueur;
        verifValeurCarteJoueur(); // Appel de la fonction permettant de vérifier les valeurs des cartes du joueur.
        break;
        case carte4Joueur:
        tirerUneCarte();
        carte4Joueur = carte;
        valeurTotCarteJoueur += carte4Joueur;
        verifValeurCarteJoueur(); // Appel de la fonction permettant de vérifier les valeurs des cartes du joueur.
        break;
        case carte5Joueur:
        tirerUneCarte();
        carte5Joueur = carte;
        valeurTotCarteJoueur += carte5Joueur;
        verifValeurCarteJoueur(); // Appel de la fonction permettant de vérifier les valeurs des cartes du joueur.
        break;    
        case carte6Joueur:
        tirerUneCarte();
        carte6Joueur = carte;
        valeurTotCarteJoueur += carte6Joueur;
        verifValeurCarteJoueur(); // Appel de la fonction permettant de vérifier les valeurs des cartes du joueur.
        break;
        case carte7Joueur:
        tirerUneCarte();
        carte7Joueur = carte;
        valeurTotCarteJoueur += carte7Joueur;
        verifValeurCarteJoueur(); // Appel de la fonction permettant de vérifier les valeurs des cartes du joueur.
        break;
        case carte8Joueur:
        tirerUneCarte();
        carte8Joueur = carte;
        valeurTotCarteJoueur += carte8Joueur;
        verifValeurCarteJoueur(); // Appel de la fonction permettant de vérifier les valeurs des cartes du joueur.
        break;
        case carte9Joueur:
        tirerUneCarte();
        carte9Joueur = carte;
        valeurTotCarteJoueur += carte9Joueur;
        verifValeurCarteJoueur(); // Appel de la fonction permettant de vérifier les valeurs des cartes du joueur.
        break;
        case carte10Joueur:
        tirerUneCarte();
        carte10Joueur = carte;
        valeurTotCarteJoueur += carte10Joueur;
        verifValeurCarteJoueur(); // Appel de la fonction permettant de vérifier les valeurs des cartes du joueur.
        break;
    }
    
}

function croupierAutreCarte() { // Fonction permettant de faire tirer une autre carte au croupier suivant des conditions.
    
    tirerUneCarte();
    
    valTotCrouHypo = 0;
    valTotCrouHypo += valeurTotCarteCroupier;
    valTotCrouHypo += carte;
    
    if(valTotCrouHypo < 17) {
        
        if(carte < 2) {
            
            if((16 < valTotCrouHypo) && (valTotCrouHypo < 22)) {
                
                valeurTotCarteCroupier += 11;
                
            }
            
            if(valTotCrouHypo < 17) {
                valeurTotCarteCroupier += carte;
            }
            
        }
        
        if(carte > 1) {
            
            valeurTotCarteCroupier += carte;
            
        }
        
    }
    
    if((valTotCrouHypo > 16) && (carte > 1)) {
        
        valeurTotCarteCroupier += carte;
        
        if(valeurTotCarteCroupier > 21)  {
            console.log("Partie finie :");
            console.log(`La valeur du total des cartes du croupier est : ${valeurTotCarteCroupier}`);
            console.log("le croupier a perdu");
            console.log(`La valeur du total des cartes du joueur est : ${valeurTotCarteJoueur}`);
            console.log("Le joueur a gagné");
            console.log(`Pour démarrer une nouvelle partie, taper : nouvellePartie();`); // Appel de la fonction permettant de démarrer une partie.
        }
        
        if(valeurTotCarteCroupier < 22) {
            
            if(valeurTotCarteCroupier > valeurTotCarteJoueur) {
                console.log("Partie finie :");
                console.log(`La valeur du total des cartes du croupier est : ${valeurTotCarteCroupier}`);
                console.log("le croupier a gagné");
                console.log(`La valeur du total des cartes du joueur est : ${valeurTotCarteJoueur}`);
                console.log("Le joueur a perdu");
                console.log(`Pour démarrer une nouvelle partie, taper : nouvellePartie();`); // Appel de la fonction permettant de démarrer une partie.
            }
            
            if(valeurTotCarteCroupier < valeurTotCarteJoueur) {
                console.log("Partie finie :");
                console.log(`La valeur du total des cartes du croupier est : ${valeurTotCarteCroupier}`);
                console.log("le croupier a perdu");
                console.log(`La valeur du total des cartes du joueur est : ${valeurTotCarteJoueur}`);
                console.log("Le joueur a gagné");
                console.log(`Pour démarrer une nouvelle partie, taper : nouvellePartie();`); // Appel de la fonction permettant de démarrer une partie.
            }
            
        }
    }
    
    
    if(valeurTotCarteCroupier < 17) {
        console.log(`La valeur du total des cartes du croupier est : ${valeurTotCarteCroupier}`);
        console.log(`Taper : croupierAutreCarte();`); // Appel de la fonction permettant faire tirer une autre carte au croupier.
    }
    
    if(valeurTotCarteCroupier > 21)  {
        console.log("Partie finie :");
        console.log(`La valeur du total des cartes du croupier est : ${valeurTotCarteCroupier}`);
        console.log("le croupier a perdu");
        console.log(`La valeur du total des cartes du joueur est : ${valeurTotCarteJoueur}`);
        console.log("Le joueur a gagné");
        console.log(`Pour démarrer une nouvelle partie, taper : nouvellePartie();`); // Appel de la fonction permettant de démarrer une partie.
    }
    
}

function revelerCarteCroupier() { // Fonction permettant de révéler la deuxième carte du croupier.
    
    console.log(`Joueur a les valeurs de cartes : ${carte1Joueur} et ${carte2Joueur}`);

    // If permettant de révéler les cartes tirées du joueur.
    if(carte3Joueur != 0) {
        console.log(`Joueur a la valeur de sa troisième carte à : ${carte3Joueur}`)
    }
    if(carte4Joueur != 0) {
        console.log(`Joueur a la valeur de sa quatrième carte à : ${carte4Joueur}`)
    }
    if(carte5Joueur != 0) {
        console.log(`Joueur a la valeur de sa cinquième carte à : ${carte5Joueur}`)
    }
    if(carte6Joueur != 0) {
        console.log(`Joueur a la valeur de sa sixième carte à : ${carte6Joueur}`)
    }
    if(carte7Joueur != 0) {
        console.log(`Joueur a la valeur de sa septième carte à : ${carte7Joueur}`)
    }
    if(carte8Joueur != 0) {
        console.log(`Joueur a la valeur de sa huitième carte à : ${carte8Joueur}`)
    }
    if(carte9Joueur != 0) {
        console.log(`Joueur a la valeur de sa neuvième carte à : ${carte9Joueur}`)
    }
    if(carte10Joueur != 0) {
        console.log(`Joueur a la valeur de sa dixième carte à : ${carte10Joueur}`)
    }
    
    console.log(`La valeur du total des cartes du joueur est : ${valeurTotCarteJoueur}`);
    
    console.log(`Croupier a les cartes : ${carte1Croupier} et ${carte2Croupier}`);
    console.log(`La valeur du total des cartes du croupier est : ${valeurTotCarteCroupier}`);
    
    if(valeurTotCarteCroupier >= 17) {
        
        if(valeurTotCarteCroupier > valeurTotCarteJoueur) {
            console.log("Partie finie :");
            console.log(`La valeur du total des cartes du croupier est : ${valeurTotCarteCroupier}`);
            console.log("le croupier a gagné");
            console.log(`La valeur du total des cartes du joueur est : ${valeurTotCarteJoueur}`);
            console.log("Le joueur a perdu");
            console.log(`Pour démarrer une nouvelle partie, taper : nouvellePartie();`); // Appel de la fonction permettant de démarrer une partie.
        }
        
        if(valeurTotCarteCroupier < valeurTotCarteJoueur) {
            console.log("Partie finie :");
            console.log(`La valeur du total des cartes du croupier est : ${valeurTotCarteCroupier}`);
            console.log("le croupier a perdu");
            console.log(`La valeur du total des cartes du joueur est : ${valeurTotCarteJoueur}`);
            console.log("Le joueur a gagné");
            console.log(`Pour démarrer une nouvelle partie, taper : nouvellePartie();`); // Appel de la fonction permettant de démarrer une partie.
        }
        
    }
    
    if(valeurTotCarteCroupier <= 16) {
        
        console.log(`Taper : croupierAutreCarte();`); // Appel de la fonction permettant de faire tirer une autre carte au croupier suivant les conditions.
        
    }
    
}





function quellesCartes() { // fonction permettant de savoir quelles sont les valeurs des cartes tirées, suivant les modalités du jeu.
    
    console.log(`Joueur a les valeurs de cartes : ${carte1Joueur} et ${carte2Joueur}`);
    console.log(`La valeur du total des cartes du joueur est : ${valeurTotCarteJoueur}`);
    
    console.log(`Croupier a les cartes : ${carte1Croupier} et carte à valeur cachée`);
    
    console.log(`Si le joueur veut une autre carte, écrivez : \"joueurAutreCarte();\"`); // Appel de la fonction par le joueur lui permettant de tirer une autre carte.
    console.log(`Si le joueur ne veut pas une autre carte, écrivez : \"revelerCarteCroupier();\"`); // Appel de ma fonction par le joueur lui permettant de révéler la deuxième carte du croupier.
    
}


function deroulerPartiePremierRound() { // Fonction permettant de tirer les premières cartes.
    
    tirageDeCartes(); // Appel de la fonction permettant de tirer des cartes de valeurs aléatoires.
    
    quellesCartes(); // Appel de la fonction permettant de savoir quelles sont les valeurs des cartes tirées, suivant les modalités du jeu.
    
}


function compterAsJoueur() { // Fonction permettant au joueur de choisir la valeur de l'as.
    valeurTotCarteJoueur += 10;
    carte += 100;
    verifValeurCarteJoueur();
}





function verifValeurCarteJoueur() { // Fonction permettant de vérifier les valeurs des cartes du joueur.
    
    console.log(`Joueur a les valeurs de cartes : ${carte1Joueur} et ${carte2Joueur}`);
    
    // If permettant de révéler les cartes tirées du joueur.
    if(carte3Joueur != 0) {
        console.log(`Joueur a la valeur de sa troisième carte à : ${carte3Joueur}`)
    }
    if(carte4Joueur != 0) {
        console.log(`Joueur a la valeur de sa quatrième carte à : ${carte4Joueur}`)
    }
    if(carte5Joueur != 0) {
        console.log(`Joueur a la valeur de sa cinquième carte à : ${carte5Joueur}`)
    }
    if(carte6Joueur != 0) {
        console.log(`Joueur a la valeur de sa sixième carte à : ${carte6Joueur}`)
    }
    if(carte7Joueur != 0) {
        console.log(`Joueur a la valeur de sa septième carte à : ${carte7Joueur}`)
    }
    if(carte8Joueur != 0) {
        console.log(`Joueur a la valeur de sa huitième carte à : ${carte8Joueur}`)
    }
    if(carte9Joueur != 0) {
        console.log(`Joueur a la valeur de sa neuvième carte à : ${carte9Joueur}`)
    }
    if(carte10Joueur != 0) {
        console.log(`Joueur a la valeur de sa dixième carte à : ${carte10Joueur}`)
    }
    
    
    if(valeurTotCarteJoueur > 21) {
        console.log("Partie finie :");
        console.log("le croupier a gagné");
        console.log(`La valeur du total des cartes du joueur est : ${valeurTotCarteJoueur}`);
        console.log("Le joueur a perdu");
        console.log(`Pour démarrer une nouvelle partie, taper : nouvellePartie();`); // Appel de la fonction permettant de démarrer une partie
        
    }
    
    if(valeurTotCarteJoueur === 21) {
        console.log("Partie finie :");
        console.log("le croupier a perdu");
        console.log(`La valeur du total des cartes du joueur est : ${valeurTotCarteJoueur}`);
        console.log("Le joueur a gagné");
        console.log(`Pour démarrer une nouvelle partie, taper : nouvellePartie();`); // Appel de la fonction permettant de démarrer une partie
    }
    
    if(valeurTotCarteJoueur < 21) {
        console.log(`La valeur du total des cartes du joueur est : ${valeurTotCarteJoueur}`);
        
        console.log(`Croupier a les cartes : ${carte1Croupier} et carte à valeur cachée`);
        console.log(`Si le joueur veut une autre carte, écrivez : \"joueurAutreCarte();\"`); // Appel de la fonction par le joueur lui permettant de tirer une autre carte.


        if(carte1Joueur === 1) {

            console.log(`Le joueur a tirée un as, la valeur total des cartes du joueur est comptée comme si la carte valait 1`);
            console.log(`Si le joueur veut compter l'as comme un 11 (donc rajouté : 10), taper : compterAsJoueur();`); // Appel de la fonction permettant au joueur de choisir la valeur de l'as.
            console.log(`Si non, écrivez : \"joueurAutreCarte();\"`); // Appel de la fonction par le joueur lui permettant de tirer une autre carte.

        }


        if(carte === 1) {

            console.log(`Le joueur a tirée un as, la valeur total des cartes du joueur est comptée comme si la carte valait 1`);
            console.log(`Si le joueur veut compter l'as comme un 11 (donc rajouté : 10), taper : compterAsJoueur();`); // Appel de la fonction permettant au joueur de choisir la valeur de l'as.
            console.log(`Si non, écrivez : \"joueurAutreCarte();\"`); // Appel de la fonction par le joueur lui permettant de tirer une autre carte.

        }


        console.log(`Si le joueur ne veut pas une autre carte, écrivez : \"revelerCarteCroupier();\"`); // Appel de ma fonction par le joueur lui permettant de révéler la deuxième carte du croupier.
    }
    
}


function nouvellePartie() { // Fonction permettant de démarrer une partie

    // Les variables qui aurraient été eu une affectation de valeurs sont remises à zéro.
    
    carte1Joueur = 0;
    carte2Joueur = 0;
    carte3Joueur = 0;
    carte4Joueur = 0;
    carte5Joueur = 0;
    carte6Joueur = 0;
    carte7Joueur = 0;
    carte8Joueur = 0;
    carte9Joueur = 0;
    carte10Joueur = 0;
    
    valeurTotCarteJoueur = 0
    
    carte1Croupier = 0;
    carte2Croupier = 0;
    carte4Croupier = 0;
    carte5Croupier = 0;
    carte6Croupier = 0;
    carte7Croupier = 0;
    carte8Croupier = 0;
    carte9Croupier = 0;
    carte10Croupier = 0;
    
    valeurTotCarteCroupier = 0;
    
    carte = 0;
    
    deroulerPartiePremierRound(); // les premières cartes sont tirées.
    
}


function ouvertureDepage() { // Message de bienvenu.
    console.log(`Bienvenu sur le jeu du BlackJack`);
    console.log(`Pour démarrer une nouvelle partie, taper : nouvellePartie();`); // Appel de la fonction permettant de démarrer une partie.
}

ouvertureDepage(); // Applet de la fonction en ouvrant la page ou en la rafraichissant.

